// Assignment Operators
let variable = "Initial variable";

// += -= *= /= 
let num1 = 4;
let num2 = 5;

num1 *= num2;
console.log(num1); //20

num2 -= 2;
console.log(num2); // 3

num2 += 9;
console.log(num2); // 3 + 9 = 12

num1 /= num2;
console.log(num1); // 20 / 12 = 1.6667

// MDAS (Multiplication, Division, Addition, Subtraction)
let mdasResult = 1 + 2 - 3 * 4 / 5;
console.log(mdasResult);
/* 
    3 * 4 = 12
    12 / 5 = 2.4
    1 + 2 = 3
    3 - 2.4 = 0.6
*/

// Increment and Decrement .. adding or subtracting 1 from the variable
let x = 2;
x++; // x = x + 1;
console.log(x);
x--; // x = x - 1;
console.log(x);

// Comparison
// loose equality operator 
console.log(1 == 1);
let isSame = 55 == 55;
console.log(isSame);
console.log(1 == "1"); // true, loose equality operator priority is the sameness of the value because with loose equality operator, forced coercion is done before the comparison.

console.log(0 == false); // true, with force coercion false was converted to a number and the equivalent of false is 0

console.log(true == "true"); // false, because true converted to 1

// Strict Equality
console.log(true === "1"); // false operands have same value but different types
console.log("Amer" === "Amer"); // true
console.log(55 === "55"); // false

// Inequality Operators (!=)
    // loose inequality operators
        // Checks wether the operands are NOT equal and/or have different value
console.log("1" != 1); //false, both operands were converted to numbers 1, so 1 is equal to 1
console.log("James" != "John"); // true, because "James" is no equal to "John"
console.log(1 != true); // false, true was converted to 1, so 1 is equal to 1
console.log(true != "true"); // true, "true" was converted to 1 but the result is NAN 
console.log("0" != false) // false, "0" was converted to 0 and false was converted to 0.

// Strict inequality
console.log("5" !== 5); // true
console.log(5 !== 5); //false

// Relational Comparison Operators
   // a compare operator which will check the relationship between the operands
let a = 500;
let b = 700;
let c = 8000;
console.log(a > b); // false
console.log(c > b); // true
console.log(c < b); // false
console.log(b < b); // false
console.log(b <= b); // true

// Mini-Activity
let food = 'pizza';
let sum = 150 + 9;
let product = 100 * 90;
let isActive = false;
let restaurants = ["McDonald's", "Burger King", "Grumpy Joe", "Popeyes", "Wendy's"];
console.log(restaurants);

let singer = {
    firstName: "Adam",
    lastName: "Levine",
    stageName: "Maroon 5",
    birthDay: "Mar - 18 - 1979",
    age: 42,
    bestAlbum: "Jordi",
    bestSong: "Memories",
    isActive: true
};
console.log(singer);

function division (firstNum, secondNum) {
    return firstNum / secondNum;
};
let quotient = division(12, 6);
console.log(`The result of the division is: ${quotient}`);


// And Operator &&
let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;
// to get authorization you should be admin and registered
let authorization1 = isAdmin && isRegistered;
console.log(authorization1);
// to get authorization you should be legal age and registered
let authorization2 = isRegistered && isLegalAge;
console.log(authorization2);

let requiredLevel = 95;
let requiredAge = 18;
let authorization3 = isRegistered && requiredLevel === 25;
console.log(authorization3); // false

// Or Operator ||
let authorization4 = isAdmin || requiredAge >= 20 || requiredLevel >= 95;
console.log(authorization4); // true 

// Not operator !
// turn the boolean to the opposite value
let authorization5 = !isAdmin || !isLegalAge;
console.log(authorization5); // true

// if-else
let userName = "crusader_1998";
let userLevel = 25;
let userAge = 20;

if (userName.length > 10) {
    console.log("Welcome to the Game");
};

if (userName.length > 10 && isRegistered && isAdmin) {
    console.log("thank you for joining admin");
} else {
    console.log("You too strong to be noob :(");
}

if (userName.length >= 10 && userLevel <= 25 && userAge >= 18) {
    console.log("Thank you for joining the noobies guild");
} else if (userLevel > 25) {
    console.log("You too strong to be a noob :(");
} else if (userAge < 18) {
    console.log("You are too young to join thr guild.");
} else if (userName.length < 10) {
    console.log("Username too short");
};

function addNum (firstNum, secondNum) {
    if (typeof firstNum === "number" && typeof secondNum === "number") {
        console.log("Run only if both arguments passed are number types.");
    } else {
        console.log("One or both of the arguments are not numbers");
    };
};
addNum(5, 2);

function login (userName, password) {
    if (password.length >= 8 && userName.length >= 8 && typeof password === "string" && typeof userName === "string") {
        console.log("you are okay to go");
    } else if (password.length < 8) {
        alert("password too short");
    } else if (userName.length < 8) {
        alert("username too short");
    } else {
        alert("one or both of the argument are not string")
    };
}
// login("AmerECE", "02136222");

// Mini-Activity

function shirtColor (day) {
    if (day === "monday".toLowerCase() && typeof day === "string") {
        alert(`${day}: wear black shirt`);
    } else if (day === "tuesday".toLowerCase()) {
        alert (`${day}: wear green shirt`);
    } else if (day === "wednesday".toLowerCase()) {
        alert(`${day}: wear yellow shirt`);
    } else if (day === "thursday".toLowerCase()) {
        alert(`${day}: wear red shirt`);
    } else if (day === "friday".toLowerCase()) {
        alert(`${day}: wear violet shirt`);
    } else if (day === "saturday".toLowerCase()) {
        alert(`${day}: wear blue shirt`);
    } else if (day === "sunday".toLowerCase()) {
        alert(`${day}: wear white shirt`);
    } else {
        alert("Invalid Input. Please use input string. Or a correct day spelling.");
    }
};

// shirtColor("friday");

// ---------------------------------
// Switch Statement
// is an alternative to an if-else ,, where the data evaluated or checked is of an expected input.
// to select one of code blocks/statements to execute
// if a case matches the given expression pr condition, then the statement for that case will run/execute
// if dose not match it will check for the next case, if no cases match, then the default code block will be executed. if we do have match, the statement for the case will run/execute.

// let role = "Admin";
function roleChecker (role) {
    switch (role) {
        case "Admin":
            console.log("Welcome Admin, to the Dashboard");
            break;
        case "User":
            console.log("You are not authorized to view this page");
            break;
        case "Guest":
            console.log("Go to the registration page to register.");
            break;
        default:
            console.log("Invalid Role. Please try again")
            break;
    };
};
roleChecker("Admin");

// Ternary Operators 
/*
    a short hand way of writing if-else statement
    ? if statement
    : else statement
else statement is required
*/
let price = 5000;
/*
if (price > 1000) {
    console.log("price is over 1000");
} else {
    console.log("price is less than 1000")
};
*/

price > 1000 ? console.log("price over 1000") : console.log("price is less than 1000");

let hero = "Goku";
hero === "Vegeta" ? console.log("your are the prince") : console.log("you are not even a royalty");

let villain = "Harvey Dent";
/* villain === "Harvey Dent" ? console.log("You were suppose to be the chosen."); */
villain === "Two Face"
? console.log("You live long enough to be a villain")
: console.log("Not quite villainous yet.");

let robin1 = "Grayson";
let currentRobin = "Tim Drake";

let isFirstRobin = currentRobin === robin1 ? true : false;
console.log(isFirstRobin);

// else-if with Ternary operator
let y = 7;

y === 5 
? console.log("Y")
: (y === 10 ? console.log("Y is 10") : console.log("Y is not 5 or 10"));
