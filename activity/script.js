// Odd or Even Checker
function oddEvenChecker (number) {
    (number === 0 && typeof number === "number")
    ? console.log(`The number ${number}: is even.`)
    : (number % 2 === 0 && typeof number === "number" ? console.log(`The number ${number}: is even.`)
    : (number % 2 !== 0 && typeof number === "number" ? console.log(`The number ${number}: is odd.`)
    : alert("Invalid Input.")));
};
oddEvenChecker(4);


// Budget Checker
function budgetChecker (budget) {
    (budget > 40000 && typeof budget === "number")
    ? console.log("You are over the budget.")
    : (budget < 40000 && typeof budget === "number" ? console.log("You have resources left.")
    : alert("Invalid Input."));
};
budgetChecker(41000);